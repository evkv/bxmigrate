<?php

namespace evkv\bxmigrate\tests\bxmigrate\repo;

use evkv\bxmigrate\tests\BaseCase;
use evkv\bxmigrate\cli\Factory;

class FactoryTest extends BaseCase
{
    public function testRegisterCommands()
    {
        $app = $this->getMockBuilder('\\Symfony\\Component\\Console\\Application')
            ->disableOriginalConstructor()
            ->getMock();

        $app->expects($this->at(0))
            ->method('add')
            ->with($this->isInstanceOf('\\evkv\\bxmigrate\\cli\\SymphonyUp'));
        $app->expects($this->at(1))
            ->method('add')
            ->with($this->isInstanceOf('\\evkv\\bxmigrate\\cli\\SymphonyDown'));
        $app->expects($this->at(2))
            ->method('add')
            ->with($this->isInstanceOf('\\evkv\\bxmigrate\\cli\\SymphonyCreate'));
        $app->expects($this->at(3))
            ->method('add')
            ->with($this->isInstanceOf('\\evkv\\bxmigrate\\cli\\SymphonyRefresh'));
        $app->expects($this->at(4))
            ->method('add')
            ->with($this->isInstanceOf('\\evkv\\bxmigrate\\cli\\SymphonyCheck'));

        $res = Factory::registerCommands($app, sys_get_temp_dir());

        $this->assertInstanceOf('\\Symfony\\Component\\Console\\Application', $res);
    }
}
