<?php

namespace evkv\bxmigrate;

/**
 * Базовое исключение, от которого унаследованы все исключения в библиотеке.
 */
class Exception extends \Exception
{
}
