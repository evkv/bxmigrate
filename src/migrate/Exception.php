<?php

namespace evkv\bxmigrate\migrate;

/**
 * Исключение, которое будет выброшено во время работы миграции.
 */
class Exception extends \evkv\bxmigrate\Exception
{
}
